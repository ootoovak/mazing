import Color exposing (lightGreen)
import Graphics.Collage exposing (circle, outlined, solid, move, rotate, collage, toForm)
import Graphics.Element exposing (Element, layers, show, color, container, middle, bottomLeft)
import Mouse exposing (clicks, position)
import Window exposing (dimensions)

main : Signal Element
main =
  Signal.map2 scene Window.dimensions clickLocations


clickLocations : Signal (List (Int,Int))
clickLocations =
  Signal.foldp (::) [] (Signal.sampleOn Mouse.clicks Mouse.position)


      -- |> filled (hsla (toFloat x) 0.9 0.6 0.7)
scene : (Int,Int) -> List (Int,Int) -> Element
scene (w,h) locs =
  let drawCircle (x,y) =
    circle 100
      |> outlined (solid lightGreen)
      |> move (toFloat x - toFloat w / 2, toFloat h / 2 - toFloat y)
  in
    layers
      [ collage w h (List.map drawCircle locs)
      , container w h bottomLeft (color lightGreen (container 300 50 middle (show "Click to stamp a circle.")))
      ]
